#include <iostream>
#include <string>
#include <vector>
#include <cxxopts.hpp>

enum STATUS {
    OK,
    ENCODE_ERROR,
    DECODE_ERROR,
    IO_ERROR = -1
};

bool encode(FILE *src, FILE *secret, FILE *dst);
bool decode(FILE *src, FILE *dst);

int main(int argc, char **argv)
{
    cxxopts::Options options("stgr", "Steganography example");
    options.add_options()
            ("d", "decode")
            ("e", "encode")
            ("cont", "container", cxxopts::value<std::string>())
            ("secret", "secret image", cxxopts::value<std::string>())
            ("o, output", "output", cxxopts::value<std::string>());
    auto result = options.parse(argc, argv);
    FILE *src = fopen(result["cont"].as<std::string>().c_str(), "r");
    FILE *dst = fopen(result["output"].as<std::string>().c_str(), "w+");
    if (result["e"].as<bool>() || !result["d"].as<bool>()) {
        FILE *secret = fopen(result["secret"].as<std::string>().c_str(), "r");
        if (src && secret && dst) {
            return IO_ERROR;
        }
        if (!encode(src, secret, dst)) {
            return ENCODE_ERROR;
        }
    } else {
        if (src && dst) {
            return IO_ERROR;
        }
        if (!decode(src, dst)) {
            return DECODE_ERROR;
        }
    }
    return OK;
}

bool encode(FILE *src, FILE *secret, FILE *dst)
{
    char sign[2] = {0};
    int offset = -1;
    fread(sign, sizeof(sign), 1, src);
    if (sign[0] != 'B' || sign[1] != 'M') {
        std::cerr
                << "Container image signature mismatch"
                << std::endl;
        return false;
    }

    fread(sign, sizeof(sign), 1, secret);
    if (sign[0] != 'B' || sign[1] != 'M') {
        std::cerr
                << "Secret image signature mismatch"
                << std::endl;
        return false;
    }
    int size_src = -1, size_sec = -1;
    fread(&size_src, 4, 1, src);
    fread(&size_sec, 4, 1, secret);
    if (size_src <= 0 || size_sec != size_src) {
        std::cerr
                << "Size mismatch"
                << std::endl;
        std::cerr
                << "Size cont = "
                << size_src
                << std::endl
                << "Size secret = "
                << size_sec
                << std::endl;
        return false;
    }
    fseek(src, 8, SEEK_CUR);
    fread(&offset, 4, 1, src);
    char *out_hdr = new char[offset];
    fseek(src, 0, SEEK_SET);
    fread(out_hdr, offset, 1, src);
    int c = 0;
    while (c < offset) {
        c += fwrite(out_hdr, 1, offset, dst);
    }

    fseek(src, offset, SEEK_SET);
    fseek(secret, offset, SEEK_SET);
    unsigned char byteA, byteB, byteC;
    for (int i = offset; i < size_src; i++) {
        fread(&byteA, 1, 1, src);
        fread(&byteB, 1, 1, secret);
        byteA &= 0b11111100;
        byteB >>= 6;
        byteC = byteA | byteB;
        fwrite(&byteC, 1, 1, dst);
    }
    fclose(src);
    fclose(secret);
    fclose(dst);
    return true;
}

bool decode(FILE *src, FILE *dst)
{
    char sign[2] = {0};
    int offset = -1;
    fread(sign, sizeof(sign), 1, src);
    if (sign[0] != 'B' || sign[1] != 'M') {
        std::cerr
                << "Container image signature mismatch"
                << std::endl;
        return false;
    }
    int size_src = -1;
    fread(&size_src, 4, 1, src);

    fseek(src, 8, SEEK_CUR);
    fread(&offset, 4, 1, src);
    char *out_hdr = new char[offset];
    fseek(src, 0, SEEK_SET);
    fread(out_hdr, offset, 1, src);
    int c = 0;
    while (c < offset) {
        c += fwrite(out_hdr, 1, offset, dst);
    }

    fseek(src, offset, SEEK_SET);
    unsigned char byteA, byteC;
    for (int i = offset; i < size_src; i++) {
        fread(&byteA, 1, 1, src);
        byteC = (byteA << 6) & 0xFF;
        fwrite(&byteC, 1, 1, dst);
    }
    fclose(src);
    fclose(dst);
    return true;
}
